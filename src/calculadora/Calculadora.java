package calculadora;

public class Calculadora {
	public static double Suma(double a ,double b) {
		return a+b;
	}
	public static double Resta(double a ,double b) {
		return a-b;
	}
	public static double Multiplicacion(double a ,double b) {
		return a*b;
	}
	public static double Division(double a ,double b) {
		if(b==0) {
			return 0;
		}
		double div =  Math.rint((a/b)*1000)/1000; //se toman 3 decimales para divisiones con
		                                          // con muchos decimales
		
		return div;
	}
	
	
}

