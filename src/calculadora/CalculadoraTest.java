package calculadora;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

//*Listado de requerimientos operacion Suma
//-Suma de numeros positivos (x)
//-Suma de numeros negativos (x)
//-Suma de numeros decimales (x)
//*Listado de requerimientos operacion Resta
//-Resta de numeros positivos (x)
//-Resta de numeros negativos (x)
//-Resta de numeros decimales (x)
//*Listado de requerimientos operacion Multiplicacion
//-Multiplicacion de numeros positivos (x)
//-Multiplicacion de numeros negativos (x)
//-Multiplicacion de numeros decimales (x)
//-Multiplicacion de numero decimal y entero (x)
//*Listado de requerimientos operacion Division
//-Division de numeros positivos (x)
//-Division de numeros negativos (x)
//-Division de numero y 0 (x)
//-Division de 0 y numero (x)
//-Division de 0 y 0 (x)

class CalculadoraTest {
	
	@Test
	public void SumaTest() {
		double resultado = Calculadora.Suma(2.5,3.2);
		double esperado = 5.7;
		assertEquals(esperado,resultado);
	}
	@Test
	public void RestaTest() {
		double resultado = Calculadora.Resta(8.3,-9.7);
		double esperado = 18;
		assertEquals(esperado,resultado);
	}
	@Test
	public void MultiplicacionTest() {
		double resultado = Calculadora.Multiplicacion(0,7);
		double esperado = 0;
		assertEquals(esperado,resultado);
	}
	@Test
	public void DivisionTest() {
		double resultado = Calculadora.Division(0,0);
		double esperado = 0; // division no exite asi que se tomara el 0 por defecto
		assertEquals(esperado,resultado);
	}
	
}
